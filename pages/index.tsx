import styles from "@/styles/Home.module.css";
import SignOutButton from "@/components/SignOutButton";
import Layout from "@/components/Layout";

const Home = () => {
  return (
    <Layout>
      <div>Dashboard</div>
      <SignOutButton />
    </Layout>
  );
};

export default Home;
