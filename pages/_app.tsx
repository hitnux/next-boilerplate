import "@/styles/globals.css";
import { Inter } from "next/font/google";
import { MsalProvider } from "@azure/msal-react";
import { msalInstance } from "@/services/msal.js";
import { AppProps } from "next/app";

const inter = Inter({ subsets: ["latin"] });

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <MsalProvider instance={msalInstance}>
      <style jsx global>{`
        html {
          font-family: ${inter.style.fontFamily};
        }
      `}</style>
      <Component {...pageProps} />
    </MsalProvider>
  );
};

export default App;
