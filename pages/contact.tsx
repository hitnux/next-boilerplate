import React from "react";
import { Formik, Form, Field, FormikProps } from "formik";
import Layout from "@/components/Layout";

const formData = [
  {
    name: "name",
    type: "text",
    value: "",
    placeholder: "Name",
  },
  {
    name: "surname",
    type: "text",
    value: "",
    placeholder: "Surname",
  },
  {
    name: "email",
    type: "email",
    value: "",
    placeholder: "email@mail.com",
  },
  {
    name: "gender",
    type: "select",
    value: "",
    options: [
      {
        name: "Select Gender",
        value: "",
      },
      {
        name: "Male",
        value: "male",
      },
      {
        name: "Female",
        value: "female",
      },
    ],
  },
  {
    name: "Contract",
    value: false,
    type: "checkbox",
  },
];

const getFormData = () => {
  const obj = {} as any;
  formData.map(({ name, value }) => {
    obj[name] = value;
  });
  return obj;
};

const getField = (field: any) => {
  if (field.type === "select")
    return (
      <Field as="select" name={field.name} defaultValue={field.defaultValue} value="d">
        {field.options?.map(({ name, value, attributes }: any) => (
          <option key={`${field.name}-${value}`} value={`${value}`} {...attributes}>
            {name}
          </option>
        ))}
      </Field>
    );

  return <Field {...field} />;
};

const contact = () => {
  return (
    <Layout>
      <Formik
        initialValues={getFormData()}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props: FormikProps<any>) => (
          <Form>
            {formData.map((field) => (
              <div key={field.name}>{getField(field)}</div>
            ))}
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
};

export default contact;
