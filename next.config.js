/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: false,
  },
  /*
  async redirects() {
    return [
      {
        source: "/",
        destination: "/login",
        permanent: false,
        missing: [
          {
            type: "cookie",
            key: "authorized",
            value: "true",
          },
        ],
      },
      {
        source: "/login",
        destination: "/",
        permanent: false,
        has: [
          {
            type: "cookie",
            key: "authorized",
            value: "true",
          },
        ],
      },
    ];
  },
  */
};

module.exports = nextConfig;
