import { graphConfig, middleWareConfig } from "./msal";

/**
 * Attaches a given access token to a MS Graph API call. Returns information about the user
 * @param accessToken
 */
const callMsGraph = async (accessToken) => {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;

  headers.append("Authorization", bearer);

  return fetch(graphConfig.graphMeEndpoint, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
};

const callMiddleware = async (accessToken) => {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;

  headers.append("Authorization", bearer);

  return fetch(middleWareConfig.middleWareEndpoint, {
    method: "GET",
    headers: headers,
  })
    .then((response) => response.json())
    .catch((error) => console.log(error));
};

export { callMiddleware, callMsGraph };
