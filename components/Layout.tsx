import React from "react";
import { AuthenticatedTemplate, UnauthenticatedTemplate } from "@azure/msal-react";
import Login from "@/components/Login";
import { Props } from "@/types/pageType";

const Layout = ({ children }: Props) => {
  return (
    <main className="container">
      <AuthenticatedTemplate>{children}</AuthenticatedTemplate>
      <UnauthenticatedTemplate>
        <Login />
      </UnauthenticatedTemplate>
    </main>
  );
};

export default Layout;
