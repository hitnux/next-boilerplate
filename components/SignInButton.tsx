import { useMsal } from "@azure/msal-react";
import { loginRequest } from "@/services/msal";

const SignInButton = () => {
  const { instance } = useMsal();
  const handleLogin = () => {
    instance.loginRedirect(loginRequest);
  };

  return <button onClick={handleLogin}>Sign in</button>;
};

export default SignInButton;
