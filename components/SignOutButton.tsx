import React from "react";
import { useMsal } from "@azure/msal-react";
import { logoutRequest } from "@/services/msal";

const SignOutButton = () => {
  const { instance } = useMsal();
  const handleLogout = () => {
    instance.logoutRedirect(logoutRequest);
  };

  return <button onClick={handleLogout}>Sign Out</button>;
};

export default SignOutButton;
